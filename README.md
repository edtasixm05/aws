# Curs aws
## @edt ASIX-M05 Curs 2021-2022


### Apunts i pràctiques de AWS


#### HowTo ASIX


 * [HowTo-ASIX-Amazon-AWS-EC2-AMI-Cloud.pdf](https://gitlab.com/edtasixm05/aws/-/blob/main/HowTo-ASIX-Amazon-AWS-EC2-AMI-Cloud.pdf)
   Document HowTo global d'explicació del funcionament de l'entorn AWS EC2 per aprendre a practicar i treballar amb Amazon.
   Aquest és el document principal per estudiar els temes a treballar en aquest entorn.

 * [HowTo-ASIX-Amazon-AWS-EC2-Projectes](https://gitlab.com/edtasixm05/aws/-/blob/main/HowTo-ASIX-Amazon-AWS-EC2-Projectes.pdf)
   Conjunt de pràctiques d'aprenentatge de AWS Cloud orientades com a projectes, per poder seguir la moda !@xllamp# de 
   que s'aprèn fent ABP.

---

 * [Pràctica-AWS.pdf](https://gitlab.com/edtasixm05/aws/-/blob/main/Pr%C3%A0ctica-AWS.pdf)
   Resum dels exercicis que hem fet a l'aula per practicar AWS.

 * [Labs-proposats.pdf](https://gitlab.com/edtasixm05/aws/-/blob/main/Labs-proposats.pdf)
   Labs proposats com a exercicis telemàtics per fer a casa 

---
 
 * [HowTo-ASIX-AWS-CLI.pdf](https://gitlab.com/edtasixm05/aws/-/blob/main/HowTo-ASIX-AWS-CLI.pdf)
   Descripció de la instal·lació i funcionament del CLI, intèrpret d'ordres tipus shell, de AWS.

 * [HowTo-ASIX-AWS-EC2-Pràctiques.pdf](https://gitlab.com/edtasixm05/aws/-/blob/main/HowTo-ASIX-AWS-EC2-Practiques.pdf)
   Pràctiques bàsiques a realitzar per practicar la creació i tutilització de màquines virtuals en l'entorn AWS.
   Aquest document és del Curs 2021-2022 i recull les pràctiques més bàsiques fetes a l'aula.

 * [descripcio_cloud_ASIX.pdf](https://gitlab.com/edtasixm05/aws/-/blob/main/descripcio_cloud_ASIX.pdf)
   Resum de les pràctiques que es fan a ASIX als mòduls M06/M11 Administració de S.O. / Seguretat usant tecnologies Cloud
   com Docker i AWS.

 * [HowTo-ASIX-Terraform](https://gitlab.com/edtasixm05/aws/-/blob/main/HowTo-ASIX-Terraform.pdf)
   Utilització de l'eina *Terraform* i *Packer* per desenvolupar desplegaments al Cloud de manera programistica, en 
   scripts de Terraform com a eina de *IaaS Infrastructure as a service*. Utilització de Packer per generar de manera
   desatesa imagtes AMI de màquines virtuals al Cloud.


 * [HowTo-ASIX-Vagrant](https://gitlab.com/edtasixm05/aws/-/blob/main/HowTo-ASIX-Vagrant.pdf)
   Document que explora la tecnologia de creació i desplegament de màquines virtuals de manera programística usant Vagrant.


---

# Accés AWS Academy

```
usuari@correu.com.awsacademy

https://www.awsacademy.com/SiteLogin (només per a instructors)
https://www.awsacademy.com/LMS_Login (només per a students)
```



